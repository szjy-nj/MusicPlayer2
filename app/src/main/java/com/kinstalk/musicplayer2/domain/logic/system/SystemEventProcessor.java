package com.kinstalk.musicplayer2.domain.logic.system;

import android.util.Log;

import com.kinstalk.musicplayer2.data.source.tasks.TasksRepository;
import com.kinstalk.musicplayer2.domain.DomainContext;
import com.kinstalk.musicplayer2.util.Injection;

/**
 * Created by libin on 2016/8/30.
 */

public class SystemEventProcessor {
    private static final String TAG = "SystemEventProcessor";
    private static SystemEventProcessor INSTANCE = null;
    private final TasksRepository mTasksRepository;

    public enum SystemEvent {
        EVENT_BOOT_COMPLETE
    }

    public static synchronized SystemEventProcessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SystemEventProcessor();
        }
        return INSTANCE;
    }
    private SystemEventProcessor() {
        mTasksRepository = Injection.provideTasksRepository(DomainContext.getInstance().getContext());
    }


    public void onSystemEvent(SystemEvent what) {
        onSystemEvent(what, null);
    }
    public void onSystemEvent(SystemEvent what, Object param) {
        Log.d(TAG, "onSystemEvent: what - " + what + " param - " + param);
        switch (what) {
            case EVENT_BOOT_COMPLETE:
                mTasksRepository.refreshTasks();
                break;
            default:
                Log.w(TAG, "onSystemEvent: unknown event - " + what);
                break;
        }
    }
}
