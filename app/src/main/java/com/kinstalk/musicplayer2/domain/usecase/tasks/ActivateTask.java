/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kinstalk.musicplayer2.domain.usecase.tasks;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kinstalk.musicplayer2.data.source.tasks.TasksRepository;
import com.kinstalk.musicplayer2.domain.usecase.UseCase;
import com.kinstalk.musicplayer2.util.Injection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Marks a task as active (not completed yet).
 */
public class ActivateTask extends UseCase<ActivateTask.RequestValue, ActivateTask.ResponseValue> {

    private final TasksRepository mTasksRepository;

    public ActivateTask(@NonNull TasksRepository tasksRepository) {
        mTasksRepository = checkNotNull(tasksRepository, "tasksRepository cannot be null!");
    }

    @Override
    protected void executeUseCase(final RequestValue values) {
        String activeTask = values.getActivateTask();
        mTasksRepository.activateTask(activeTask);
        getUseCaseCallback().onResponse(values, new ResponseValue());
    }

    public static final class RequestValue extends UseCase.RequestValue {

        private final String mActivateTask;

        public RequestValue(@NonNull String activateTask) {
            mActivateTask = checkNotNull(activateTask, "activateTask cannot be null!");
        }

        public String getActivateTask() {
            return mActivateTask;
        }

        @Override
        public UseCase getUseCase(Context context) {
            return Injection.provideActivateTask(context);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("ActivateTask.RequestValue {");
            sb.append("super=" + super.toString());
            sb.append("mActivateTask=" + mActivateTask);
            sb.append("}");
            return sb.toString();
        }
    }

    public static final class ResponseValue extends UseCase.ResponseValue {

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("ActivateTask.ResponseValue {");
            sb.append("super=" + super.toString());
            sb.append("}");
            return sb.toString();
        }
    }
}
