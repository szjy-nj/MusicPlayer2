package com.kinstalk.musicplayer2.domain.eventhub;

import android.content.Context;
import android.util.Log;

import com.kinstalk.musicplayer2.domain.usecase.UseCase;
import com.kinstalk.musicplayer2.domain.usecase.UseCaseHandler;
import com.kinstalk.musicplayer2.util.Injection;
import com.kinstalk.musicplayer2.util.QLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by libin on 2016/8/25.
 */
public class DomainEventHub {
    private static final boolean TRACE_REQUEST = true;
    private static final boolean TRACE_RESPONSE = true;

    private static DomainEventHub INSTANCE;

    private final UseCaseHandler mUseCaseHandler;
    private Context mContext;

    private DomainEventHub(Context context) {
        mUseCaseHandler = Injection.provideUseCaseHandler();
        mContext = context.getApplicationContext();
        EventBus.getDefault().register(this);
    }

    public static synchronized void init(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DomainEventHub(context);
        }
    }

    @Subscribe
    public void onEvent(UseCase.RequestValue request) {
        QLog.traceEvent(DomainEventHub.this, request);
        mUseCaseHandler.execute(request.getUseCase(mContext), request,
                        new UseCase.UseCaseCallback<UseCase.RequestValue, UseCase.ResponseValue>() {
                    @Override
                    public void onResponse(UseCase.RequestValue request, UseCase.ResponseValue response) {
                        // FORCE link response with request!!
                        response.setRequest(request);
                        QLog.traceEvent(DomainEventHub.this, response);
                        EventBus.getDefault().post(response);
                    }
                });
    }
}
