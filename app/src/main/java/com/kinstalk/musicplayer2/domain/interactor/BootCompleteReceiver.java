package com.kinstalk.musicplayer2.domain.interactor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kinstalk.musicplayer2.domain.usecase.system.BootCompleteCase;

import org.greenrobot.eventbus.EventBus;

public class BootCompleteReceiver extends BroadcastReceiver {
    private static final String TAG = "BootCompleteReceiver";

    public BootCompleteReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: Boot complete!");
        EventBus.getDefault().post(new BootCompleteCase.RequestValue());
    }
}
