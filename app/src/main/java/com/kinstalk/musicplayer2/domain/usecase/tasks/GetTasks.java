/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kinstalk.musicplayer2.domain.usecase.tasks;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kinstalk.musicplayer2.data.model.tasks.Task;
import com.kinstalk.musicplayer2.data.source.tasks.TasksDataSource;
import com.kinstalk.musicplayer2.data.source.tasks.TasksRepository;
import com.kinstalk.musicplayer2.domain.logic.tasks.filter.FilterFactory;
import com.kinstalk.musicplayer2.domain.logic.tasks.filter.TaskFilter;
import com.kinstalk.musicplayer2.domain.logic.tasks.filter.TasksFilterType;
import com.kinstalk.musicplayer2.domain.usecase.UseCase;
import com.kinstalk.musicplayer2.util.Injection;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Fetches the list of tasks.
 */
public class GetTasks extends UseCase<GetTasks.RequestValue, GetTasks.ResponseValue> {

    private final TasksRepository mTasksRepository;

    private final FilterFactory mFilterFactory;

    public GetTasks(@NonNull TasksRepository tasksRepository, @NonNull FilterFactory filterFactory) {
        mTasksRepository = checkNotNull(tasksRepository, "tasksRepository cannot be null!");
        mFilterFactory = checkNotNull(filterFactory, "filterFactory cannot be null!");
    }

    @Override
    protected void executeUseCase(final RequestValue values) {
        if (values.isForceUpdate()) {
            mTasksRepository.refreshTasks();
        }

        mTasksRepository.getTasks(new TasksDataSource.LoadTasksCallback() {
            @Override
            public void onTasksLoaded(List<Task> tasks) {
                TasksFilterType currentFiltering = values.getCurrentFiltering();
                TaskFilter taskFilter = mFilterFactory.create(currentFiltering);

                List<Task> tasksFiltered = taskFilter.filter(tasks);
                ResponseValue responseValue = new ResponseValue(tasksFiltered);
                getUseCaseCallback().onResponse(values, responseValue);
            }

            @Override
            public void onDataNotAvailable() {
                ResponseValue responseValue = new ResponseValue(UseCase.ResponseValue.ERROR_NULL_RESULT);
                getUseCaseCallback().onResponse(values, responseValue);
            }
        });

    }

    public static final class RequestValue extends UseCase.RequestValue {

        private final TasksFilterType mCurrentFiltering;
        private final boolean mForceUpdate;

        public RequestValue(boolean forceUpdate, @NonNull TasksFilterType currentFiltering) {
            mForceUpdate = forceUpdate;
            mCurrentFiltering = checkNotNull(currentFiltering, "currentFiltering cannot be null!");
        }

        @Override
        public UseCase getUseCase(Context context) {
            return Injection.provideGetTasks(context);
        }

        public boolean isForceUpdate() {
            return mForceUpdate;
        }

        public TasksFilterType getCurrentFiltering() {
            return mCurrentFiltering;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("GetTasks.RequestValue {");
            sb.append("super=" + super.toString());
            sb.append(" mForceUpdate=" + mForceUpdate);
            sb.append(" mCurrentFiltering=" + mCurrentFiltering);
            sb.append("}");
            return sb.toString();
        }
    }

    public static final class ResponseValue extends UseCase.ResponseValue {

        private final List<Task> mTasks;

        public ResponseValue(int error) {
            mError = error;
            mTasks = null;
        }

        public ResponseValue(@NonNull List<Task> tasks) {
            mTasks = checkNotNull(tasks, "tasks cannot be null!");
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("GetTasks.ResponseValue {");
            sb.append("super=" + super.toString());
            sb.append(" mTasks=" + mTasks);
            sb.append("}");
            return sb.toString();
        }

        public List<Task> getTasks() {
            return mTasks;
        }
    }
}
