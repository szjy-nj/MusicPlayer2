package com.kinstalk.musicplayer2.domain;

import android.content.Context;
import android.util.Log;

import com.kinstalk.musicplayer2.domain.eventhub.DomainEventHub;

/**
 * Created by libin on 2016/8/30.
 */

public class DomainContext {
    private static final String TAG = "DomainContext";
    private static DomainContext INSTANCE = null;
    private Context mContext;

    private DomainContext(Context context) {
        if (context != null) {
            mContext = context.getApplicationContext();
            DomainEventHub.init(mContext);
        } else {
            Log.w(TAG, "DomainContext: context is NULL!!");
        }
    }

    public static synchronized void init(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DomainContext(context);
        }
    }

    public static synchronized DomainContext getInstance() {
        return INSTANCE;
    }

    public Context getContext() {
        return mContext;
    }
}
