package com.kinstalk.musicplayer2.domain.usecase.system;

import android.content.Context;

import com.kinstalk.musicplayer2.domain.logic.system.SystemEventProcessor;
import com.kinstalk.musicplayer2.domain.usecase.UseCase;
import com.kinstalk.musicplayer2.util.Injection;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by libin on 2016/8/30.
 */

public class BootCompleteCase extends UseCase<BootCompleteCase.RequestValue, BootCompleteCase.ResponseValue> {


    public BootCompleteCase() {
    }

    @Override
    protected void executeUseCase(final BootCompleteCase.RequestValue values) {
        SystemEventProcessor.getInstance().onSystemEvent(
                SystemEventProcessor.SystemEvent.EVENT_BOOT_COMPLETE);
        getUseCaseCallback().onResponse(values, new BootCompleteCase.ResponseValue());
    }

    public static final class RequestValue extends UseCase.RequestValue {
        public RequestValue() {
        }

        @Override
        public UseCase getUseCase(Context context) {
            return Injection.provideBootCompleteCase(context);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("BootCompleteCase.RequestValue {");
            sb.append("super=" + super.toString());
            sb.append("}");
            return sb.toString();
        }
    }

    public static final class ResponseValue extends UseCase.ResponseValue {

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("BootCompleteCase.ResponseValue {");
            sb.append("super=" + super.toString());
            sb.append("}");
            return sb.toString();
        }
    }
}
