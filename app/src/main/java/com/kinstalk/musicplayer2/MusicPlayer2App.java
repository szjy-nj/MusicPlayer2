package com.kinstalk.musicplayer2;

import android.app.Application;

import com.kinstalk.musicplayer2.domain.DomainContext;
import com.kinstalk.musicplayer2.domain.eventhub.DomainEventHub;

/**
 * Top-level Application class
 */
public class MusicPlayer2App extends Application {

    public MusicPlayer2App() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DomainContext.init(this.getApplicationContext());
    }
}

