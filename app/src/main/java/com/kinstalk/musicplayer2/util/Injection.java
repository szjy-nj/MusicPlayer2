/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kinstalk.musicplayer2.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.kinstalk.musicplayer2.data.source.tasks.TasksDataSource;
import com.kinstalk.musicplayer2.data.source.tasks.TasksRepository;
import com.kinstalk.musicplayer2.data.source.tasks.local.TasksLocalDataSource;
import com.kinstalk.musicplayer2.data.source.tasks.remote.TasksRemoteDataSource;
import com.kinstalk.musicplayer2.domain.logic.tasks.filter.FilterFactory;
import com.kinstalk.musicplayer2.domain.usecase.UseCaseHandler;
import com.kinstalk.musicplayer2.domain.usecase.tasks.ActivateTask;
import com.kinstalk.musicplayer2.domain.usecase.system.BootCompleteCase;
import com.kinstalk.musicplayer2.domain.usecase.tasks.ClearCompleteTasks;
import com.kinstalk.musicplayer2.domain.usecase.tasks.CompleteTask;
import com.kinstalk.musicplayer2.domain.usecase.tasks.GetTasks;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Enables injection of production implementations for
 * {@link TasksDataSource} at compile time.
 */
public class Injection {

    public static TasksRepository provideTasksRepository(@NonNull Context context) {
        checkNotNull(context);
        return TasksRepository.getInstance(TasksRemoteDataSource.getInstance(),
                TasksLocalDataSource.getInstance(context));
    }

    public static GetTasks provideGetTasks(@NonNull Context context) {
        return new GetTasks(provideTasksRepository(context), new FilterFactory());
    }

    public static UseCaseHandler provideUseCaseHandler() {
        return UseCaseHandler.getInstance();
    }

    public static CompleteTask provideCompleteTasks(Context context) {
        return new CompleteTask(Injection.provideTasksRepository(context));
    }

    public static ActivateTask provideActivateTask(Context context) {
        return new ActivateTask(Injection.provideTasksRepository(context));
    }

    public static ClearCompleteTasks provideClearCompleteTasks(Context context) {
        return new ClearCompleteTasks(Injection.provideTasksRepository(context));
    }

    public static BootCompleteCase provideBootCompleteCase(Context context) {
        return new BootCompleteCase();
    }
}