/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kinstalk.musicplayer2.presentation.tasks;

import android.support.annotation.NonNull;

import com.kinstalk.musicplayer2.data.model.tasks.Task;
import com.kinstalk.musicplayer2.data.source.tasks.TasksDataSource;
import com.kinstalk.musicplayer2.domain.logic.tasks.filter.TasksFilterType;
import com.kinstalk.musicplayer2.domain.usecase.UseCase;
import com.kinstalk.musicplayer2.domain.usecase.tasks.ActivateTask;
import com.kinstalk.musicplayer2.domain.usecase.tasks.ClearCompleteTasks;
import com.kinstalk.musicplayer2.domain.usecase.tasks.CompleteTask;
import com.kinstalk.musicplayer2.domain.usecase.tasks.GetTasks;
import com.kinstalk.musicplayer2.util.QLog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to user actions from the UI ({@link TasksFragment}), retrieves the data and updates the
 * UI as required.
 */
public class TasksPresenter implements TasksContract.Presenter {


    private final TasksContract.View mTasksView;
    private TasksFilterType mCurrentFiltering = TasksFilterType.ALL_TASKS;
    private boolean mFirstLoad = true;

    public TasksPresenter(@NonNull TasksContract.View tasksView) {
        mTasksView = checkNotNull(tasksView, "tasksView cannot be null!");

        mTasksView.setPresenter(this);
    }

    @Override
    public void start() {
        EventBus.getDefault().register(this);
        loadTasks(false);
    }

    @Override
    public void stop() {
        EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void onEvent(UseCase.ResponseValue responseValue) {
        QLog.traceEvent(TasksPresenter.this, responseValue);
        if (responseValue instanceof GetTasks.ResponseValue) {
            GetTasks.ResponseValue response = (GetTasks.ResponseValue) responseValue;
            if (response.isError()) {
                mTasksView.showLoadingTasksError();
            } else {
                List<Task> tasks = response.getTasks();
                // The view may not be able to handle UI updates anymore
                if (!mTasksView.isActive()) {
                    return;
                }
                mTasksView.setLoadingIndicator(false);

                processTasks(tasks);
            }
        } else if (responseValue instanceof CompleteTask.ResponseValue
                || responseValue instanceof ActivateTask.ResponseValue
                || responseValue instanceof ClearCompleteTasks.ResponseValue) {
            if (responseValue.isError()) {
                mTasksView.showLoadingTasksError();
            } else {
                mTasksView.showTaskMarkedComplete();
                loadTasks(false, false);
            }
        }

    }

    @Override
    public void result(int requestCode, int resultCode) {
        // If a task was successfully added, show snackbar
//        if (AddEditTaskActivity.REQUEST_ADD_TASK == requestCode
//                && Activity.RESULT_OK == resultCode) {
//            mTasksView.showSuccessfullySavedMessage();
//        }
    }

    @Override
    public void loadTasks(boolean forceUpdate) {
        // Simplification for sample: a network reload will be forced on first load.
        loadTasks(forceUpdate || mFirstLoad, true);
        mFirstLoad = false;
    }

    /**
     * @param forceUpdate   Pass in true to refresh the data in the {@link TasksDataSource}
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    private void loadTasks(boolean forceUpdate, final boolean showLoadingUI) {
        if (showLoadingUI) {
            mTasksView.setLoadingIndicator(true);
        }

        GetTasks.RequestValue requestValue = new GetTasks.RequestValue(forceUpdate,
                mCurrentFiltering);
        EventBus.getDefault().post(requestValue);
    }

    private void processTasks(List<Task> tasks) {
        if (tasks.isEmpty()) {
            // Show a message indicating there are no tasks for that logicunit type.
            processEmptyTasks();
        } else {
            // Show the list of tasks
            mTasksView.showTasks(tasks);
            // Set the logicunit label's text.
            showFilterLabel();
        }
    }

    private void showFilterLabel() {
        switch (mCurrentFiltering) {
            case ACTIVE_TASKS:
                mTasksView.showActiveFilterLabel();
                break;
            case COMPLETED_TASKS:
                mTasksView.showCompletedFilterLabel();
                break;
            default:
                mTasksView.showAllFilterLabel();
                break;
        }
    }

    private void processEmptyTasks() {
        switch (mCurrentFiltering) {
            case ACTIVE_TASKS:
                mTasksView.showNoActiveTasks();
                break;
            case COMPLETED_TASKS:
                mTasksView.showNoCompletedTasks();
                break;
            default:
                mTasksView.showNoTasks();
                break;
        }
    }

    @Override
    public void addNewTask() {
        mTasksView.showAddTask();
    }

    @Override
    public void openTaskDetails(@NonNull Task requestedTask) {
        checkNotNull(requestedTask, "requestedTask cannot be null!");
        mTasksView.showTaskDetailsUi(requestedTask.getId());
    }

    @Override
    public void completeTask(@NonNull Task completedTask) {
        checkNotNull(completedTask, "completedTask cannot be null!");
        EventBus.getDefault().post(new CompleteTask.RequestValue(
                completedTask.getId()));
    }

    @Override
    public void activateTask(@NonNull Task activeTask) {
        checkNotNull(activeTask, "activeTask cannot be null!");
        EventBus.getDefault().post(new ActivateTask.RequestValue(
                activeTask.getId()));
    }

    @Override
    public void clearCompletedTasks() {
        EventBus.getDefault().post(new ClearCompleteTasks.RequestValue());
    }

    @Override
    public TasksFilterType getFiltering() {
        return mCurrentFiltering;
    }

    /**
     * Sets the current task filtering type.
     *
     * @param requestType Can be {@link TasksFilterType#ALL_TASKS},
     *                    {@link TasksFilterType#COMPLETED_TASKS}, or
     *                    {@link TasksFilterType#ACTIVE_TASKS}
     */
    @Override
    public void setFiltering(TasksFilterType requestType) {
        mCurrentFiltering = requestType;
    }

}
